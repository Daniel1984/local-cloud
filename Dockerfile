# start from the latest golang image as a builder
FROM golang:1.16.3 as builder

# create working directory
WORKDIR /app

# receive scope argument that will allow to reuse same
# Dockerfile for multiple lambdas
ARG SCOPE

# copy package definition and download all dependencies
COPY ./go.mod ./go.sum ./
RUN go mod download

# download rest of source
COPY . .

# build lambda for stage 2 / Emulator to use it
RUN CGO_ENABLED=0 GOOS=linux go build -o app cmd/${SCOPE}/main.go

#----- Start Stage 2 of the build -----

# satrt new stage to emulate lambda
FROM public.ecr.aws/lambda/go:latest

# set working dir as per docs
WORKDIR ${LAMBDA_TASK_ROOT}

# copy function code from builder
COPY --from=builder /app/app .

# Set the CMD to your handler (could also be done as a parameter override outside of the Dockerfile)
CMD [ "app" ]
