## Testing Lambdas Locally

There's RIE tool (Lambda Runtime Interface Emulator)
- https://github.com/aws/aws-lambda-runtime-interface-emulator
- https://docs.aws.amazon.com/lambda/latest/dg/images-test.html

Above allows to use public AWS lambda go runtime and execute handlers as they were deployed to cloud.

### Instructions
- `doker-compose` is used to orchestrate the launch of lambdas.
- this example has 2 lambdas `cmd/lambda_one` and `cmd/lambda_two`
- To add additional lambda, just copy/paste one of existing `docker-compose.yml` services and **IMPORTANT:** set unique IP (just increment by 1)
- `SCOPE` arg should be pointing to lambda directory name in `cmd` allowing to reuse same `Dockerfile` for multiple lambdas
- run example: `docker-compose up`
- Updating lambdas - amend go code, kill `docker-compose` and rerun it `docker-compose up --build --remove-orphans`
- Call lambdas (in this example 2 lambdas running. 1st on port 9000 and 2nd on port 9001):
  - calling 1st lambda: `curl -XPOST "http://localhost:9000/2015-03-31/functions/function/invocations" -d '{"payload":"hello world!"}'`
  - calling 2st lambda: `curl -XPOST "http://localhost:9001/2015-03-31/functions/function/invocations" -d '{"payload":"hello world!"}'`

### TODO
- add `amazon/dynamodb-local` to `docker-compose.yml` and share access via `.env` with lambdas
- others?

### Improvements
- to avoid killing and restarting `docker-compose` to see changes, need a way to implement `CompileDaemon` or similar tool to listen for changes in source and rebuild within the container.
