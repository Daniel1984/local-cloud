package main

import (
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/cloudtesting/pkg/respwriter"
)

func main() {
	lambda.Start(Run)
}

func Run(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	foo, ok := os.LookupEnv("FOO")
	if !ok {
		return respwriter.RespondWithOK([]byte("LAMBDA 1 - env var not found")), nil
	}
	return respwriter.RespondWithOK([]byte(fmt.Sprintf("LAMBDA 1 - ENV var found and it's: %s", foo))), nil
}
